<?php

class ConnectionList
{
	public $list;
	// con - discon
	private $lastEvent;
	private $currentKey;
	function __construct()
	{
		$this->list = array ();
		$this->lastEvent = 'n';
	}
	public function addConnect(string $string)
	{
		// first detected connect in the logs
		if ($this->lastEvent == 'n')
		{
			$this->currentKey = uniqid ();
			$this->list [$this->currentKey] [] = $string;
			$this->lastEvent = 'con';
		}
		// no double connects
		elseif ($this->lastEvent == 'con')
		{
			// remove previous, failed connection from our array
			unset ( $this->list [$this->currentKey] );
			
			$this->currentKey = uniqid ();
			$this->list [$this->currentKey] [] = $string;
			$this->lastEvent = 'con';
		}
		// previous connection was sucsessfully started and terminated, open new connection span
		elseif ($this->lastEvent == 'discon')
		{
			$this->currentKey = uniqid ();
			$this->list [$this->currentKey] [] = $string;
			$this->lastEvent = 'con';
		}
	}
	public function addDisconnect(string $string)
	{
		if ($this->lastEvent == 'con')
		{
			$this->list [$this->currentKey] [] = $string;
			$this->lastEvent = 'discon';
		}
	}
}
?>