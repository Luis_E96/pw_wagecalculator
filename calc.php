<?php
$guid = $_POST ['guid'];
$logDate = explode ( "-", $_POST ['date'] );
$urlDate = $logDate [1] . '_' . $logDate [2] . '_' . substr ( $logDate [0], 2, 2 );

$fHnd = file ( "http://eu.pw-phoenix.com/servers/1/logs//server_log_{$urlDate}.txt" );

$output = array (
		'connected' => array (),
		'disconnected' => array () 
);

$searchString1 = "has joined the game with ID: $guid and has administrator rights.";
$searchString2 = "(GUID: $guid) has left the server";

require_once 'connectionList.class.php';
require_once 'wageCalculator.class.php';
$myConMgr = new ConnectionList ();

foreach ( $fHnd as $num => $line )
{
	// connect
	if (strpos ( $line, $searchString1 ) !== false)
	{
		
		$myConMgr->addConnect ( $line );
	}
	// disconnect
	elseif (strpos ( $line, $searchString2 ) !== false)
	{
		$myConMgr->addDisconnect ( $line );
	}
}

new wageCalc ( $myConMgr );
?>