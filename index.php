<html>
<head>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" href="main.css">

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script type="text/javascript">
$(document).ready(function(){
	
	console.log( "Document ready" );

	$(".loader").hide();
	
});
$( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker" ).datepicker("option", "dateFormat", "yy-mm-dd");
    $( "#datepicker" ).addClass("inputdate");
  } );
function calc()
{
	$(".loader").show();
	var date = $(".date").val();
	var guid = $("#guid").val();

	console.log(date);
	console.log(guid);
	
	$.ajax(
			{
				url: "calc.php",
				method: "POST",
				data: {date: date, guid: guid}, 
				success: function(result){
					
        			$(".result-wrapper").html(result);
        			$(".loader").hide();
    			}
    		}
	    );
}


</script>

</head>
<body>
	<div id="content_wrapper">
		<div class="input-wrapper">
			<div class="guid">
				<div class="guid text">GUID:</div>
				<input type="text" id="guid">
			</div>
			<button class="submit" onClick="calc()">Calculate</button>
		</div>
		<div class="input-wrapper">
			<input type="text" class="date" id="datepicker">
		</div>
		<div class="loader"></div>
		<div class="result-wrapper"></div>
	</div>
</body>
</html>